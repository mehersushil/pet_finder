<?php

use Controller\PetController;
use Service\Container;

require "../bootstrap.php";

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );

if ($uri[1] !== 'pet') {
    header("HTTP/1.1 404 Not Found");
    exit();
}

$petId = null;
if (isset($uri[2])) {
    $petId = (int) $uri[2];
}

$container = new Container();
$requestMethod = $_SERVER["REQUEST_METHOD"];
$controller = new PetController($container, $requestMethod, $petId);
$controller->processRequest();
