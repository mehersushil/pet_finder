<?php


namespace Controller;


use Model\Location;
use Model\Pet;
use Service\Container;

class PetController
{
    private $petRepository;
    private $requestMethod;
    private $petId;

    public function __construct(Container $container, $requestMethod, $petId)
    {
        $this->requestMethod = $requestMethod;
        $this->petId = $petId;
        $this->petRepository = $container->getPetRepository();
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->petId) {
                    $response = $this->getPet($this->petId);
                } else {
                    $response = $this->getAllPets();
                }
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getPet($id)
    {
        $pet = $this->petRepository->find($id);
        if (!$pet) {
            return $this->notFoundResponse();
        }

        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($this->createPetFromData($pet));
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }

    public function createPetFromData($petData)
    {
        $pet = new Pet();
        $pet->id = $petData['id'];
        $pet->name = $petData['name'];
        $pet->breed = $petData['breed'];
        $pet->age = $petData['age'];
        $pet->personality = $petData['personality'];
        $pet->createdAt = $petData['created_at'];
        $pet->updatedAt = $petData['updated_at'];

        $location = new Location();
        $location->name = $petData['location'];
        $location->address = $petData['address'];
        $location->city = $petData['city'];
        $location->state = $petData['state'];
        $location->zip = $petData['zip'];
        $location->county = $petData['county'];
        $location->phone = $petData['phone'];

        $pet->shelter = $location;

        return $pet;
    }

    private function getAllPets()
    {
        $petsArr = $this->petRepository->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $pets = [];
        foreach ($petsArr as $pet) {
            $pets[] = $this->createPetFromData($pet);
        }

        $response['body'] = json_encode($pets);
        return $response;
    }
}