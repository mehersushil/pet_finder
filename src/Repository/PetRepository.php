<?php


namespace Repository;


use PDO;
use PDOException;

class PetRepository
{
    /**
     * @var PDO
     */
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function findAll()
    {
        $statement = "
            SELECT p.id, p.name, p.breed, p.age, p.personality, l.name, l.address, l.city, l.state, l.zip,
            l.county, l.phone
            FROM pets p
            INNER JOIN locations l ON l.id = p.shelter_id
        ";

        try {
            $statement = $this->pdo->query($statement);

            return $statement->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function find($id)
    {
        $statement = "
            SELECT p.id, p.name, p.breed, p.age, p.personality, l.name, l.address, l.city, l.state, l.zip,
            l.county, l.phone 
            FROM pets p
            INNER JOIN locations l ON l.id = p.shelter_id
            WHERE p.id = :id;
        ";

        try {
            $statement = $this->pdo->prepare($statement);
            $statement->bindParam(':id', $id, PDO::PARAM_INT);
            $statement->execute();

            return $statement->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
}