<?php


namespace Service;


use PDO;
use Repository\PetRepository;

class Container
{
    /**
     * @var PDO
     */
    private $pdo;

    /**
     * @var PetRepository
     */
    private $petRepository;

    public function __construct()
    {
    }

    /**
     * @return PetRepository
     */
    public function getPetRepository()
    {
        if ($this->petRepository === null) {
            $this->petRepository = new PetRepository($this->getPDO());
        }

        return $this->petRepository;
    }

    /**
     * @return PDO
     */
    public function getPDO()
    {
        if ($this->pdo === null) {
            $path = substr($_SERVER['DOCUMENT_ROOT'], 0, -6);
            $this->pdo = new PDO("sqlite:" . $path . $_ENV['SQLITE_FILE']);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        return $this->pdo;
    }
}