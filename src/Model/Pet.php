<?php


namespace Model;


class Pet
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $breed;

    /**
     * @var int
     */
    public $age;

    /**
     * @var string
     */
    public $personality;

    /**
     * @var Location
     */
    public $shelter;

    /**
     * @var int
     */
    public $createdAt;

    /**
     * @var int
     */
    public $updatedAt;
}